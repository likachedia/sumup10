package com.example.sumup10.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sumup10.PasswordViewModel

class ItemViewModelFactory(private val repository: Repository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return  PasswordViewModel(repository) as T
    }
}