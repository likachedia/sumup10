package com.example.sumup10

import android.graphics.Color
import android.util.Log
import android.widget.Toast
import androidx.core.graphics.drawable.toDrawable
import androidx.core.view.children
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sumup10.base.BaseFragment
import com.example.sumup10.databinding.FragmentPasswordBinding
import com.google.android.material.snackbar.Snackbar

class PasswordFragment : BaseFragment<FragmentPasswordBinding, PasswordViewModel>(FragmentPasswordBinding::inflate) {
    override fun getViewModelClass() = PasswordViewModel::class.java
    private lateinit var padapter: PassworAdapter
    private  var buttonsList:MutableList<Buttons> = mutableListOf()
    val code:MutableList<Int> = mutableListOf<Int>(0,9,3,4)
    val userCode: MutableList<Int> = mutableListOf()

    override fun start() {
        initRecycler()
        handleCode()
    }

    private fun initRecycler(){
        Log.i("mess", "coming init")
        setData()
        padapter = PassworAdapter()
        padapter.setData(buttonsList)
        binding.recycler.apply {
            Log.i("mess", "coming apl")
            adapter = padapter
            layoutManager = GridLayoutManager(requireContext(), 3)
        }
    }

    private fun handleCode() {

        padapter.userItemOnClick = { id ->
            if(id != 10 && id != 12 && userCode.size < 4) {
                userCode.add(id)
                if(userCode.size == 4) {
                    if(userCode == code) {
                        Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Failed", Toast.LENGTH_SHORT).show()
                        userCode.clear()
                        with(binding) {
                            number1.setBackgroundColor(Color.parseColor("#8E8E93"))
                            number2.setBackgroundColor(Color.parseColor("#8E8E93"))
                            number3.setBackgroundColor(Color.parseColor("#8E8E93"))
                            number4.setBackgroundColor(Color.parseColor("#8E8E93"))
                        }

                    }
                }
                when(userCode.size) {
                    1 -> binding.number1.setBackgroundColor(Color.parseColor("#32D74B"))
                    2 -> binding.number2.setBackgroundColor(Color.parseColor("#32D74B"))
                    3 -> binding.number3.setBackgroundColor(Color.parseColor("#32D74B"))
                    4 -> binding.number4.setBackgroundColor(Color.parseColor("#32D74B"))
                }
                Log.i("mess", userCode.toString())
            } else if(id == 12 && userCode.size < 4) {
                when(userCode.size) {
                    1 -> {
                        binding.number1.setBackgroundColor(Color.parseColor("#8E8E93"))
                        userCode.removeAt(0)
                    }
                    2 -> {
                        userCode.removeAt(1)
                        binding.number2.setBackgroundColor(Color.parseColor("#8E8E93"))
                    }
                    3 ->{
                        userCode.removeAt(2)
                        binding.number2.setBackgroundColor(Color.parseColor("#8E8E93"))
                    }
                    4 -> {
                        userCode.removeAt(4)
                        binding.number2.setBackgroundColor(Color.parseColor("#8E8E93"))
                    }
                }
            }

        }
    }
    private fun setData() {
        Log.i("mess", "coming set")
        buttonsList.add(
            Buttons(1, null),
        )
        buttonsList.add(
            Buttons(2, null),
        )
        buttonsList.add(
            Buttons(3, null),
        )
        buttonsList.add(
            Buttons(4, null),
        )
        buttonsList.add(
            Buttons(5, null),
        )
        buttonsList.add(
            Buttons(6, null),
        )
        buttonsList.add(
            Buttons(7, null),
        )
        buttonsList.add(
            Buttons(8, null),
        )
        buttonsList.add(
            Buttons(9, null),
        )
        buttonsList.add(
            Buttons(10, R.drawable.ic_touch__id_1),
        )
        buttonsList.add(
            Buttons(0, null),
        )
        buttonsList.add(
            Buttons(12, R.drawable.ic_backspace_1),
        )
    }
}