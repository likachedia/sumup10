package com.example.sumup10.base


import androidx.lifecycle.ViewModel
import com.example.sumup10.util.Repository


abstract class BaseViewModel(repository: Repository): ViewModel()