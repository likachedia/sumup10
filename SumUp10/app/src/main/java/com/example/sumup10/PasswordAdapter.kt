package com.example.sumup10

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup10.PassworAdapter.Companion.ICON
import com.example.sumup10.PassworAdapter.Companion.NUMBER
import com.example.sumup10.databinding.ButtonsBinding
import com.example.sumup10.databinding.IconButtonBinding

typealias UserItemOnClick = (id: Int) -> Unit

class PassworAdapter(): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val buttons: MutableList<Buttons> = mutableListOf()
    var userItemOnClick: UserItemOnClick? = null
    companion object {
        const val NUMBER = 1
        const val ICON = 4
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == NUMBER) {
            Log.i("mess", "holder")
            PasswordViewHolder(
                ButtonsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            SrcViewHolder(IconButtonBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = buttons[position]
       if(holder is PasswordViewHolder) {
           holder.onBind(model)

       } else if(holder is SrcViewHolder) {
           holder.onBind(model)
       }


    }

    override fun getItemCount() = buttons.size

    override fun getItemViewType(position: Int): Int {
        Log.i("mess", "viewtype")
        return if(buttons[position].src == null) {
            NUMBER
        } else {
            ICON
        }
    }

    inner class PasswordViewHolder(private val binding: ButtonsBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener{

        fun onBind(model:Buttons) {
            with(binding){
                number.text = model.id.toString()
            }
            binding.number.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            val id = buttons[bindingAdapterPosition].id
            Log.i("mess", id.toString())
            userItemOnClick?.invoke(id!!)
        }
    }

     inner class SrcViewHolder(private  val binding:IconButtonBinding) : RecyclerView.ViewHolder(binding.root),
         View.OnClickListener{

        fun onBind(model:Buttons) {
            binding.icon.setImageResource(model.src!!)
            binding.icon.setOnClickListener(this)
        }

         override fun onClick(p0: View?) {
             val id = buttons[bindingAdapterPosition]?.id
             userItemOnClick?.invoke(id!!)
         }
     }

    fun setData(button:List<Buttons>){
        buttons.clear();
        buttons.addAll(button);
        notifyDataSetChanged()
    }
}